/*

What directive is used by Node.js in loading the modules it needs?
Answer: require

What Node.js module contains a method for server creation?
Answer: module

What is the method of the http object responsible for creating a server using Node.js?
Answer: http.createServer()

What method of the response object allows us to set status codes and content types?
Answer: response.writeHead

Where will console.log() output its contents when run in Node.js?
Answer: server

What property of the request object contains the address' endpoint?
Answer: res.end()

*/


const http = require("http");
const port = 3000;

const server = http.createServer((request, response) =>{
	if (request.url == '/login'){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("Welcome to the login page")
	} else {
		response.writeHead(404, {'Content-Type': 'text/plain'})
		response.end("I'm sorry the page you are looking for cannot be found")
	}

})

server.listen(port);
console.log(`Server now accessible at localhost:${port}`);