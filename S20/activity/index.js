console.log("Hello World!")

//Objective 1
// Create a function called printNumbers that will loop over a number provided as an argument.
	//In the function, add a console log to display the number provided.
	//In the function, create a loop that will use the number provided by the user and count down to 0
		//In the loop, create an if-else statement:

			// If the counter number value provided is less than or equal to 50, terminate the loop and exactly show the following message in the console:
				//"The current value is at " + count + ". Terminating the loop."

			// If the counter number value is divisible by 10, skip printing the number and show the following message in the console:
				//"The number is divisible by 10. Skipping the number."

			// If the counter number  value is divisible by 5, print/console log the counter number.


/*let num = Number(prompt("Please provide a number:"));
console.log("The number you provided is " + num + ".");

function printNumbers(num){
	for(let num = 0; num >=50; num--){
		if(num % 5 ===0){
			continue;
		}
		console.log(num)
		if(num>50)
	}

}
*/

let num = prompt("Please provide a number:");
console.log("The number your provided is " + num + ".")

function printNumbers(x){
for (let x = num; x>=0; x--){

	if (x%10 === 0){
		console.log("The number is divisible by 10. Skipping the number.");
	}

	if (((x%5) === 0) && x % 10 !==0){
		console.log(x);
	}

	if (x <=50){
		console.log("The current value is at 50. Terminating the loop.");
		break;
	}
}
}

printNumbers();



//Objective 2
let string = 'supercalifragilisticexpialidocious';
console.log(string);
let filteredString = '';

for (let i = 0; i < string.length; i++){
	//console.log(string[i]);
	if(
		string[i] == "a" ||
		string[i] == "e" ||
		string[i] == "i" ||
		string[i] == "o" ||
		string[i] == "u"
		) {
		continue;
	} filteredString += string[i];
}

console.log(filteredString);



//Do not modify
//For exporting to test.js
try {
    module.exports = {
       printNumbers, filteredString
    }
} catch(err) {

}