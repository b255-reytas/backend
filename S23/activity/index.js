console.log("Hello World")

// console.log("Hello World");

//Follow the property names and spelling given in the google slide instructions.

// Create an object called trainer using object literals

	let trainer = {
		name: ("Ash Ketchum"),
		age: 10,
		friends: {
			hoenn: ["May", "Max"],
			kanto: ["Brock", "Misty"]
		},
		pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
		talk: function(){
			console.log("Pikachu! I choose you.")
		}
	}

	console.log(trainer);

	console.log("Result of dot notation:");
	console.log(trainer.name);

	console.log("Result of bracket notation:")
	console.log(trainer.pokemon)

	console.log("Result of talk method:");
	console.log(trainer.talk());

	function Pokemon(name, level){

		this.name = name;
		this.level = level;
		this.health = 2*level;
		this.attack = level;

		// Methods
		this.tackle = function(target){
			console.log(this.name + " tackles " + target.name);
			target.health -= this.attack;
			console.log(target.name + "'s health is reduced to " + target.health);
			if(target.health <=0 ){
				target.faint();
			}

		this.faint = function(){
			console.log(this.name + " fainted");
				}
			}
		}

	let Pikachu = new Pokemon("Pikachu", 12)
	let Geodude = new Pokemon("Geodude", 8)
	let Mewtwo = new Pokemon("Mewtwo", 100)

	console.log(Pikachu)
	console.log(Geodude)
	console.log(Mewtwo)

	Geodude.tackle(Pikachu)

// Initialize/add the given object properties and methods

// Properties

// Methods

// Check if all properties and methods were properly added


// Access object properties using dot notation

// Access object properties using square bracket notation

// Access the trainer "talk" method


// Create a constructor function called Pokemon for creating a pokemon


// Create/instantiate a new pokemon


// Create/instantiate a new pokemon


// Create/instantiate a new pokemon


// Invoke the tackle method and target a different object


// Invoke the tackle method and target a different object








//Do not modify
//For exporting to test.js
try{
	module.exports = {
		trainer,
		Pokemon 
	}
} catch(err) {

}