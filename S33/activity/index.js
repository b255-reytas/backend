console.log("Hello World!")

console.log(fetch('https://jsonplaceholder.typicode.com/todos'))

// 3 & 4

fetch('https://jsonplaceholder.typicode.com/todos')
  .then(response => response.json())
  .then(data => {
    let posts = data.map(todos => ({
      title: todos.title
    }));
    console.log(posts);
  })

// 5 & 6
fetch('https://jsonplaceholder.typicode.com/todos/1')
  .then(response => response.json())
  .then(data => {
    let { title, completed } = data;
    console.log(`The to do item "${title}" has a status of ${completed}.`);
  })

// 7
fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
  		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
	  	completed: false,
	  	id: 201,
	  	title: 'Created To Do List Item',
	  	userId: 1
	})
})
.then(response => response.json())
.then(data => console.log(data));

// 8
fetch('https://jsonplaceholder.typicode.com/todos/1' , {
	method: 'PUT',
	headers: {
		'Content-type': 'application/json',
	},
	body: JSON.stringify({
		dateCompleted: "Pending",
		description: "To update the my to do list with a different data structure",
		id: 1,
		status: "Pending",
		title: "Updated List Item",
		userId: 1
	})
})
.then(response => response.json())
.then(data => console.log(data))

// 9
fetch('https://jsonplaceholder.typicode.com/todos/1' , {
	method: 'PUT',
	headers: {
		'Content-type': 'application/json',
	},
	body: JSON.stringify({
		title: "New Title",
		description: "New",
		status: "Pending",
		dateCompleted: "03/28/2023",
		userId: 1
	})
})
.then(response => response.json())
.then(data => console.log(data))

// 10 & 11
fetch('https://jsonplaceholder.typicode.com/todos/1' , {
	method: 'PATCH',
	headers:{
		'Content-type': 'application/json',
	},
	body: JSON.stringify({
		completed: false,
		dateCompleted: "07/09/21",
		id: 1,
		status: "Complete",
		title: "delectus aut autem",
		userId: 1
	})
})
.then(response => response.json())
.then(data => console.log(data))

// 12
fetch('https://jsonplaceholder.typicode.com/posts/1',
	{
	method: 'DELETE'
})