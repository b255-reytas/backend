// CRUD Operations

// Insert Document (CREATE)

/*
	
	Syntax:
		Insert One Document
			db.collectionName.insertOne({
				"fieldA": "valueA",
				"fieldB": "valueB"
			});

		Insert Many Documents
			db.collectionName.insertMany([

			{
				"fieldA": "valueA",
				"fieldA": "valueA"
			}
			{
				"fieldA": "valueA",
				"fieldA": "valueA"
			}
			]);

*/

db.users.insertOne({
	"firstName": "Jane",
	"lastName": "Doe",
	"age": 21,
	"email": "janedoe@mail.com",
	"company": "none"

});

db.users.insertMany([
	{
	"firstName": "Stephen",
	"lastName": "Hawking",
	"age": 76,
	"email": "stephenhawking@mail.com",
	"company": "none"
	},

	{
	"firstName": "Neil",
	"lastName": "Armstrong",
	"age": 82,
	"email": "neilarmstrong@mail.com",
	"company": "none"
	}

	]);

// Find Documents(Read/Retreive)

db.users.find(); // This will retreive all the documents in the collection

db.users.find({
	"firstName": "Jane"
});

db.users.find({
	"firstName": "Neil",
	"age": 82
});

db.users.findOne({}); // Returns the first document in our collection

db.users.findOne({
	"firstName": "Stephen"
});

// Update Documents

db.users.insertOne({
	"firstName": "Test",
	"lastName": "Test",
	"age": 0,
	"email": "test@mail.com",
	"company": "none"
})

// Updating one document
db.users.updateOne(
	{
		"firstName": "Test"
	},

	{
		$set:{
				"firstName": "Bill",
				"lastName": "Gates",
				"age": 65,
				"email": "billgates@mail.com",
				"company": "microsoft",
				"status": "active"
		}
	}
);

// Removing a field
db.users.updateOne(
	{
		"firstName": "Bill"
	},

	{
		$unset:{
			"status": "active"
		}
	}
);

// Updating Multiple Documents

db.users.updateMany(
	{
		"company": "none"
	},

	{
		$set:{
			"company": "HR"
		}
	}
);

db.users.updateOne(
		{},
		{
			$set:{
				"company": "operations"
			}
		}
	);

db.users.updateMany(
	{},
	{
		$set:{
			"company": "comp"
		}
	}
);

// Delete Documents (Delete)

db.users.insertOne({
	"firstName": "Test"
});

db.users.deleteOne({
	"firstName": "Test"
});

db.users.deleteMany({
	"company": "comp"
});

db.courses.deleteMany({})