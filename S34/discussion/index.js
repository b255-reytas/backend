// Use the "require" directive to load the express module/package
// A "module" is a software component or a part of a program that contains one or more routines
// This is used to get the contents of the express package to be used by our application
// It also allows us access to methods and functions that will allow us to create a server
const express = require("express")

// Creating an application using express
// This creates an express application and stores this in an constant called app
// In layman's terms app is our server
const app = express()

const port = 4000;

// Setup for allowing the server to handle data from requests
// Allows your app to read json data
// Methods used from express JS are middlewares
// Middleware is a sofware that provides common services and capabilities to applications outside of what's offered by the operating system
app.use(express.json());


// Allows your app to read data from forms
// By default, information received from the URL can only be received as a string or an array
// By applying the option of "extended:true" this allows us to received information in other data types such as an object which we will use throughout our application
app.use(express.urlencoded({extended:true}));

// [SECTION] Routes
// Express has a methods corresponding to each HTTP method

app.get("/", (req, res) => {
	res.send("Hello World")
});

// [MINI-ACTIVITY]
// Our route must be named "hello"
// It must output this message "Hello from the /hello endpoint"

app.get("/hello", (req, res) => {
	res.send("Hello from the /hello endpoint");
});

// Create a POST route
app.post("/hello", (req,res) => {
	// req.body contains the contents/data of the request body
	// All the properties defined in our Postman request will be accessible with the same names
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}`)
});

// An array that will store user objects when the "/signup" route is accessed
// This will serve as our mock database

let users = [];

app.post("/signup", (req, res) => {
	console.log(req.body);

	// If contents of the "request.body" with the property "userName" and "password" is not empty
	if(req.body.userName !== '' && req.body.password !== ''){
		users.push(req.body);

		res.send(`User ${req.body.userName} successfully registered!`);
	} else{
		res.send("Please input BOTH username and password")
	}
})

// Create PUT route to change password of a specific user
app.put("/change-password", (req, res) => {

	let message = '';

	for(let i = 0; i < users.length; i++){
		// If the username provided in the postman client and the username of the current object in the loop is the same
		if (req.body.userName == users[i].userName){
			users[i].password = req.body.password;

			message = `User ${req.body.userName}'s password has been updated`

			// Breaks out of the loop once a user that matches the username provided is found
			break;
		} else {
			message = `User does not exist.`
		}
	}
	res.send(message);
	console.log(users);
})




// Tells our server to listen to the port
// If the port is accessed, we can run the server
// Returns a message to confirm that the server is running in the terminal

// if(require.main) would allow us to listen to the app directly if it is not imported to another module, it will run the app directly
// else, if it is needed to be imported, it will not run the app and instead export it to be used in another file
if(require.main === module){
	app.listen(port, () => console.log(`Server running at port ${port}`))
}

module.exports = app;