const express = require("express")
const app = express()
const port = 3000;

app.use(express.json());

// 1 & 2

app.get("/home", (req, res) => {
	res.send("Welcome to the homepage!")
});

// 3 & 4

const user = [
		{username: 'johndoe', password: 'johndoe1234'}
	]

app.get("/users", (req, res) => {
	res.send(user)
});


// 5 & 6

let users = [
		{ username: 'johndoe', password: 'johndoe1234' },
		{ username: 'james', password: 'smith' },
		{ username: 'juan', password: 'tamad' }

	];

app.delete("/delete-user", (req, res) => {
	const username = req.body.username;
	let message = '';

	if(users.length > 0){
		for (let i = 0; i < users.length; i++){
			if(users[i].username === username) {
				users.splice(i, 1);
				message = `User ${username} has been deleted.`;

			break;	
			}
		}
			if(message === ''){
				message = `User does not exist.`;
				}
			} else {
				message = `No users found`;
			}
			
	res.send(message);
})


// app
if(require.main === module){
	app.listen(port, () => console.log(`Server running at port ${port}`))
}

module.exports = app;