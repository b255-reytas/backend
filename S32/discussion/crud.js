let http = require("http");

// mock database
let directory = [
	{
		"name": "Brandon",
		"email": "brandon@mail.com"
	},
	{
		"name": "Jobert",
		"email": "jobert@mail.com"
	}
]

http.createServer(function(request, response){

	// route for returning all items upon receiving a GET request
	if(request.url == "/users" && request.method == "GET"){
		// Sets response output JSON data type
		response.writeHead(200, {'Content-Type': 'application/json'});

		// Input as to be data type 
		response.write(JSON.stringify(directory))
		response.end();
	}

	// A request object contains several parts
		// - Headers - contains information about the request context/content like what is the data type
		// - Body - contains the actual information being sent with the request
	if(request.url == "/users" && request.method == "POST"){

		let requestBody = '';

		// Data is received from the client and is processed in the data stream
		// The information provided from the request object enters a sequence called "data" the code below will be triggered
		request.on('data', function(data){
			// Assigns the data retreived from the data stream to the requestBody
			requestBody += data;
		});
		// We need this to be of data type JSON to access its properties
		request.on('end', function(){
		console.log(typeof requestBody)

		// Converts the string requestBody to JSON
		requestBody = JSON.parse(requestBody);

		// Create a new object representing the new mock database record

		let newUser = {
			"name": requestBody.name,
			"email": requestBody.email
		}

		directory.push(newUser);
		console.log(directory)

		response.writeHead(200, {'Content-Type': 'application/json'})
		response.write(JSON.stringify(newUser));
		response.end();
	})
}
}).listen(4000);

console.log('Server running at localhost:4000')