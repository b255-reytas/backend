console.log("Hello World!")


/*
	
//Note: strictly follow the variable names and function names from the instructions.

*/

function addNum(addNum1, addNum2){
	let sum = addNum1 + addNum2;
	console.log("Displayed sum of " + addNum1 + " and " + addNum2);
	console.log(sum);
};

function subNum(subNum1, subNum2){
	let difference = subNum2 - subNum1;
	console.log("Displayed difference of " + subNum2 + " and " + subNum1);
	console.log(difference);
}
	addNum(15,5);
	subNum(5,20);

function multiplyNum(product1, product2){
	let times = product1 * product2;
	console.log("The product of " + product1 + " and " + product2 + ":")
	return times;
	
};

let product = multiplyNum(50,10);
console.log(product);


function divideNum(divide1, divide2){
	let dividedBy = divide2 / divide1;
	console.log("The quotient of " + divide2 + " and " + divide1 + ":")
	return dividedBy;
	
};

let quotient = divideNum(10,50);
console.log(quotient);


function getCircleArea(radius){
	let circleArea = 3.1416 * (radius*radius)
	console.log("The result of getting the area of a circle with " + radius + " radius:");
	return circleArea;
};

let circleArea = getCircleArea(15);
console.log(circleArea);

function getAverage(num1, num2, num3, num4){
	let averageVar = (num1+num2+num3+num4)/4
	console.log("The average of " + num1 + ", " + num2 + ", " + num3 + ", " + "and " + num4 + ":");
	return averageVar;
};

let averageVar = getAverage(20,40,60,80);
console.log(averageVar);

function checkIfPassed(num5, num6){
	console.log("Is " + num5 + "/" + num6 + " a passing score?");
	return ((num5/num6) >= 0.75);
}
	let isPassed = checkIfPassed(38,50);
	console.log(isPassed);

//Do not modify
//For exporting to test.js
try {
	module.exports = {
		addNum,subNum,multiplyNum,divideNum,getCircleArea,getAverage,checkIfPassed
	}
} catch (err) {

}