db.fruits.insertMany([
  {
    name : "Apple",
    color : "Red",
    stock : 20,
    price: 40,
    supplier_id : 1,
    onSale : true,
    origin: [ "Philippines", "US" ]
  },

  {
    name : "Banana",
    color : "Yellow",
    stock : 15,
    price: 20,
    supplier_id : 2,
    onSale : true,
    origin: [ "Philippines", "Ecuador" ]
  },

  {
    name : "Kiwi",
    color : "Green",
    stock : 25,
    price: 50,
    supplier_id : 1,
    onSale : true,
    origin: [ "US", "China" ]
  },

  {
    name : "Mango",
    color : "Yellow",
    stock : 10,
    price: 120,
    supplier_id : 2,
    onSale : false,
    origin: [ "Philippines", "India" ]
  }
]);

// [SECTION] MongoDB Aggregation
/*
    - Used to generate manipulated data and perform operations to create filtered results that helps in analyzing data
    - Compared to doing CRUD operations on our data from the previous sessions, aggregation gives us access to manipulate, filter and compute for results prodiving us with information to make necessary development decisions without having to create a front-end application
*/

// Using the aggregate method

//  - The $ symbol will refer to a field name that is available in the documents that are being aggregated on

db.fruits.aggregate([
        { $match: { onSale: true}},
        { $group: { _id: "$supplier_id", total: { $sum: "$stock"}}}
  ]);

// Field projection with aggregation
// $project can be used when aggregating data to include/exclude fields from the returned results

db.fruits.aggregate([
      { $match: { onSale: true}},
      { $group: { _id: "$supplier_id", total: { $sum: "$stock"}}},
      { $project: {_id: 0}}
  ])

// Sorting aggregated results
/*
  - The $sort can be used to change the order of aggregated results
  - Providing a value of -1 will sort the aggregated results in a revised order
*/

db.fruits.aggregate([
      { $match: { onSale: true}},
      { $group: { _id: "$supplier_id", total: { $sum: "$stock"}}},
      { $sort: { total: -1}}
  ]);

// Aggregating results based on array fields
/*
  - the $undwind deconstructs an array field from a collection/field with an array value to output a result for each element
*/


db.fruits.aggregate([
    { $unwind: "$origin"}
  ]);

// Display fruit documents by their origin and the kinds of fruits that are supplied

db.fruits.aggregate([
    {$unwind: "$origin"},
    {$group: {_id: "$origin" , kinds: { $sum : 1}}}
  ]);


// [SECTION] Guidelines on Schema Design
/*
    - Schema design/data modelling is an important feature when creating databases
    - MongoDB documents can be categorized into normalized and de-normalized/embeded data
    - Normalized data refers to a data structure where documents are referred to each other using their ID's for related pieces of information
    - De-normalized data/embedded data refers to a data structure where related pieces of information is added to a document as an embedded object
    - Both data structures are common practice but each of them have their pros or cons
    - Normalized data makes it easier to read information because separate documents can be retrieved but in terms of querying results, it performs slower compared to embeded data due to having to retreive multiple documents at the same time
    - This approach is recommended for data structures where pieces of information are commonly operated/changed
    - De-normalized data/embeded data makes it easier for query documents and has faster performance because only one query needs to be done in order to retreive documents. However, if the data structure becomes too complex it makes it more difficult to manipulate and access information
*/

// One-to-One relationship
// Creates an id and stores it in the variable owner for use in document creation

var owner = ObjectId();

db.owners.insert({
    _id: owner,
    name: "John Smith",
    contact: "0987654321"
});

// Change the "owner_id" using the actual id in the previously created document

db.suppliers.insert({
  name: "ABC fruits",
  contact: "1234567890",
  owner_id: owner
})

// One-To Few Relationship

db.suppliers.insert({
  name: "DEF fruits",
  contact: "1234567890",
  addresses: [
      { street: "123 San Jose St", city: "Manila"},
      { street: "367 Gil Puyat", city: "Makati"}
    ]
});