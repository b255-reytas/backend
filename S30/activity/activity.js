
/*2*/
db.fruits.aggregate([
		{ $match: {onSale: true}},
		{ $group: {_id: 0 , fruitsOnSale: { $sum: 1}}}
	])


/*3*/
db.fruits.aggregate([
		{ $match: {stock: { $gte: 20}}},
		{ $group: {_id: 0 , enoughStock: { $sum: 1}}}
	])

/*4*/
db.fruits.aggregate([
		{ $match: { onSale: true}},
		{ $group: { _id: "$supplier_id" , avg_price: {$avg: "$price"}}}
	])

/*5*/
db.fruits.aggregate([
		{ $match: { onSale: true}},
		{ $group: { _id: "$supplier_id" , max_price: {$max: "$price"}}}
	])


/*6*/

db.fruits.aggregate([
		{ $match: { onSale: true}},
		{ $group: { _id: "$supplier_id" , min_price: {$min: "$price"}}}
	])
