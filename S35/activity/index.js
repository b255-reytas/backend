const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 3001;

mongoose.connect("mongodb+srv://ryan-255:admin123@zuitt-bootcamp.hbsy0g5.mongodb.net/?retryWrites=true&w=majority",
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}
);

let db = mongoose.connection

db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("We're connected to the cloud database"))

const userSchema = new mongoose.Schema({
	username: {
		type: String,
		required: true,
		unique: true
	},
	password:{
		type: String,
		required: true
	}
})

const User = mongoose.model("User", userSchema);

app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.post("/signup", (req,res) => {
	const {username, password} = req.body;

	if(!username || !password){
		res.send(`BOTH username and password must be provided.`)
		return;
	}

	User.findOne({ username }).then((result, err) => {
		if(result){
			res.send(`Duplicate username found`)
		}
		else {
			const newUser = new User({
				username: req.body.username,
				password: req.body.password
			});
			newUser.save().then((savedUser) => {
				console.log(`New usersaved: ${savedUser}`);
				res.send(`New user registered`)
			})
		}
	})
})

if(require.main === module){
	app.listen(port, () => console.log(`Server running at port ${port}`));

}

module.exports = app;