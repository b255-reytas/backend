const express = require("express");
const router = express.Router();
const userController = require("../controllers/user");
const auth = require("../auth");


// Route for user registration
/* router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
}); */

// Route for user registration version 2
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController =>{
		if(resultFromController){
			res.send(`User registered successfully. Welcome to LaSiyapi!`)
		} else {
			res.send(`Registration failed due to duplicate email. Please try another email`)
		}
	})
});


// Route for user authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});

// ----- S45 -----

router.post("/checkout", auth.verify, (req, res) => {
	
	const userData = auth.decode(req.headers.authorization);
	let data = {
		userId: userData.id, 
		productId: req.body.productId
	}

	userController.checkout(data, req.body.productId).then(resultFromController => res.send(resultFromController))
})


router.get("/:userId", (req, res) => {
	console.log(req.params.userId);
	userController.getUser(req.params).then(resultFromController => res.send(resultFromController))
});


// ----- Stretch Goals -----

// ----- Set user as admin (Admin Only) -----

router.put("/:userId", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin == true){
		userController.updateUser(req.params.userId).then((resultFromController) => res.send(resultFromController))
	} else {
		res.send(false);
	}
})

// ----- Retreive authenticated user's orders -----



// Allows us to export the "router" object that will be acessed in our "index.js" file
module.exports = router;