const express = require("express");
const router = express.Router();
const productController = require("../controllers/product");
const auth = require("../auth");

router.post("/", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin == true){
		console.log(req.body)
		productController.addProduct(req.body).then((resultFromController) => res.send(resultFromController))
	} else {
		res.send(`Unauthorized access`);
	}
})


router.get("/all", (req, res) => {
	productController.gettAllProducts().then(resultFromController => res.send(resultFromController))
});


router.get("/", (req, res) => {
	productController.getAllActive().then(resultFromController => res.send(resultFromController))
});


// ----- S44 -----

router.get("/:productId", (req, res) => {
	console.log(req.params.productId);
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController))
});

router.put("/:productId", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin == true){
		productController.updateProduct(req.params, req.body).then((resultFromController) => res.send(resultFromController))
	} else {
		res.send(`Unauthorized access`);
	}
})

router.put("/:productId/archive", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin == true){
		productController.archiveProduct(req.params).then((resultFromController) => res.send(resultFromController))
	} else {
		res.send(`Unauthorized access`);
	}
})

// ----- Stretch Goals -----













module.exports = router;