const Product = require("../models/product");

module.exports.addProduct = (reqBody) => {
	let newProduct = new Product({
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	});

	return newProduct.save().then((product, error) => {
		// Course creation failed
		if(error) {
			return false;

		// Course creation successful
		} else {
			return `Product ${product.name} ${product.description} successfully added.`
		}
	})
}

module.exports.gettAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	})
}

module.exports.getAllActive = () => {
	return Product.find({isActive : true}).then(result => {
		return result;
	})
}


// ----- S44 -----


// -- get specific product --
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result
	})
}

// -- update a specific product --
module.exports.updateProduct = (reqParams, reqBody) => {

	let updateProduct = {
		name : reqBody.name,
		description : reqBody.description,
		isActive : reqBody.isActive,
		price : reqBody.price
	}

	return Product.findByIdAndUpdate(reqParams.productId, updateProduct).then((product, error) => {
		if (error){
			return false;
		} else {
			return `Successfully updated Item: ${product.name}`;
		}
	})
}

// -- archive a specific product --
module.exports.archiveProduct = (reqParams) => {

	let updateActiveField = {
		isActive : false
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {
		if (error) {
			return false;
		} else {
			return `Item ${product.name} is successfully archived`;
		}
	});
};


// ----- Stretch Goals -----