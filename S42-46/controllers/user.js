const User = require("../models/user");
const Product = require("../models/product");
const bcrypt = require("bcrypt");
const auth = require("../auth");


/* module.exports.registerUser = (reqBody) =>{
	let newUser = new User({
		email : reqBody.email,
		password : bcrypt.hashSync(reqBody.password, 10)
	})
	return newUser.save().then((user, error) =>{
		if(error) {
			return `Registration failed.`;
		} else {
			return `Registration success. Welcome to LaSiyapi!`
		};
	})


} */

// Registration version 2

module.exports.registerUser = (reqBody) =>{
	return User.findOne({email: reqBody.email}).then(existingUser => {
		if(existingUser) {
			// Duplicate email found, so return false
			return false;
		} else {
			// No duplicate email, so create a new user
			let newUser = new User({
				email : reqBody.email,
				password : bcrypt.hashSync(reqBody.password, 10)
			})
			return newUser.save().then((user, error) =>{
				if(error) {
					return false;
				} else {
					return true;
				}
			})
		}
	})
}

/* module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result => {
		if(result == null){
			return `Incorrect email or password. Please try again.`;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if(isPasswordCorrect) {
				return {access : auth.createAccessToken(result)}
			} else {
				return false;
			}
		}
	})
} */

// Login version 2
module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result => {
		if(result == null){
			return `Incorrect email or password. Please try again.`;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if(isPasswordCorrect) {
				const accessToken = auth.createAccessToken(result);
				return `Successfully logged in. You may use this access token: (${accessToken})`
			} else {
				return false;
			}
		}
	})
}

// ----- S45 -----

module.exports.checkout = async (data) => {
	let isUserUpdated = await User.findById(data.userId).then(user => {
		return user.save().then((user, error) => {
			if(error){
				return false;
			} else {
				return true;
			}
		})
	})

	let isProductUpdated = await Product.findById(data.productId).then(product => {

		if (product == null){
			return false;
		}
		product.userOrders.push({userId : data.userId});
		return product.save().then((product, error) => {
			if(error){
				return false;
			} else {
				return true;
			}
		})
	})
	if(isUserUpdated && isProductUpdated){
		return `Item successfully ordered. Please anticipate delivery within 1 to 3 days`;
	} else {
		return `Checkout failed.`
	}
};

module.exports.getUser = (reqParams) => {
	return User.findById(reqParams.userId).then(result => {
		return result
	})
}


// ----- Stretch Goals -----

// ----- Set user as admin (Admin Only)

module.exports.updateUser = (userId) => {

	let updateUser = {
		isAdmin : true
	}

	return User.findByIdAndUpdate(userId, updateUser).then((user, error) => {
		if (error){
			return false;
		} else {
			return true;
		}
	})
}