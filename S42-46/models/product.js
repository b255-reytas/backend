const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
		name : {
			type : String,
			required : [true, "Name is required"]
		},
		description : {
			type : String,
			required : [true, "Description is required"]
		},
		price : {
			type : Number,
			default : 0

		},
		isActive : {
			type : Boolean,
			default : true
		},
		createdOn : {
			type : Date,
			default : new Date()
		},
		userOrders : [
			{
				userId : {
					type: String,
					required : [true, "User ID is required"]
				},
				orderId : {
					type: String,
					/* required : [true, "Order ID is required"] */
				}
			}
		]
})

	module.exports = mongoose.model("Order", orderSchema);